/*
    main.cpp - Main program for the Invaders example.

    Copyright (C) 2010 David Boddie <david@boddie.org.uk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
    Requires that the touchscreen has been calibrated and the calibration
    matrix has been written to the Arduino's EEPROM using TFT_lib's
    touchpanel.save_matrix() method.
*/

#include "WProgram.h"
#include <TFT_ILI9325.h>
#include <Touchpanel.h>
#include <stdlib.h>
#include <avr/pgmspace.h>

#define TITLE_X1 (120 - 64)
#define TITLE_Y1 16
#define TITLE_X2 (120 - 80)
#define TITLE_Y2 36
#define TITLE_X3 120
#define TITLE_Y3 36
#define SW0 8
#define SW 16
#define SH0 8
#define SH 16
#define ROW_SPACING 24
#define ROW_HEIGHT 24
#define DOUBLE_ROW_HEIGHT 48
#define Y 288
#define BULLETS 2
#define ALIENS 16
#define ALIEN_BULLETS 4
#define ALIEN_ROWS 4
#define ROW_ITEMS 4

touchpanel tp;

// 16-bit colour
// components are f8   (red)
//                 7e  (green)
//                  1f (blue)

// Top to bottom, left to right

PROGMEM uint8_t chars[29][7] = {
    {0x7c, 0x82, 0x82, 0xfe, 0x82, 0x82, 0x82}, // A
    {0xf8, 0x84, 0x84, 0xfc, 0x82, 0x82, 0xfc}, // B
    {0x7c, 0x82, 0x80, 0x80, 0x80, 0x82, 0x7c}, // C
    {0xf0, 0x88, 0x84, 0x82, 0x84, 0x88, 0xf0}, // D
    {0xfe, 0x80, 0x80, 0xfc, 0x80, 0x80, 0xfe}, // E
    {0xfe, 0x80, 0x80, 0xfc, 0x80, 0x80, 0x80}, // F
    {0x7c, 0x82, 0x80, 0x80, 0x8e, 0x84, 0x7c}, // G
    {0x82, 0x82, 0x82, 0xfe, 0x82, 0x82, 0x82}, // H
    {0x7c, 0x10, 0x10, 0x10, 0x10, 0x10, 0x7c}, // I
    {0x7c, 0x10, 0x10, 0x10, 0x10, 0x90, 0x60}, // J
    {0x82, 0x84, 0x88, 0xf0, 0x88, 0x84, 0x82}, // K
    {0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0xfe}, // L
    {0x82, 0xc6, 0xaa, 0x92, 0x82, 0x82, 0x82}, // M
    {0x82, 0xc2, 0xa2, 0x92, 0x8a, 0x86, 0x82}, // N
    {0x7c, 0x82, 0x82, 0x82, 0x82, 0x82, 0x7c}, // O
    {0xfc, 0x82, 0x82, 0xfc, 0x80, 0x80, 0x80}, // P
    {0x7c, 0x82, 0x82, 0x82, 0x8a, 0x86, 0x7e}, // Q
    {0xfc, 0x82, 0x82, 0xfc, 0x88, 0x84, 0x82}, // R
    {0x7c, 0x82, 0x80, 0x7c, 0x02, 0x82, 0x7c}, // S
    {0xfe, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10}, // T
    {0x82, 0x82, 0x82, 0x82, 0x82, 0x82, 0x7c}, // U
    {0x82, 0x82, 0x44, 0x44, 0x28, 0x28, 0x10}, // V
    {0x82, 0x82, 0x82, 0x54, 0x54, 0x28, 0x28}, // W
    {0x82, 0x44, 0x28, 0x10, 0x28, 0x44, 0x82}, // X
    {0x82, 0x44, 0x28, 0x10, 0x10, 0x10, 0x10}, // Y
    {0xfe, 0x04, 0x08, 0x10, 0x20, 0x40, 0xfe}, // Z
    {0x10, 0x30, 0x70, 0xfe, 0x70, 0x30, 0x10}, // backspace
    {0x02, 0x04, 0x08, 0x08, 0x90, 0x50, 0x20}, // end
    {0xfe, 0x00, 0xfe, 0x00, 0xfe, 0x00, 0xfe}  // cursor
};

PROGMEM uint8_t title1[8] = {'I', 'N', 'V', 'A', 'D', 'E', 'R', 'S'};
PROGMEM uint8_t title2[4] = {'F', 'R', 'O', 'M'};
PROGMEM uint8_t title3[5] = {'S', 'P', 'A', 'C', 'E'};

PROGMEM uint8_t title4[5] = {'E', 'N', 'T', 'E', 'R'};
PROGMEM uint8_t title5[4] = {'Y', 'O', 'U', 'R'};
PROGMEM uint8_t title6[4] = {'N', 'A', 'M', 'E'};

PROGMEM uint8_t numbers[10][7] = {
    {0x7c, 0x82, 0x82, 0x92, 0x82, 0x82, 0x7c},
    {0x10, 0x30, 0x50, 0x10, 0x10, 0x10, 0x7c},
    {0x7c, 0x82, 0x02, 0x0c, 0x30, 0x40, 0xfe},
    {0x7c, 0x82, 0x02, 0x3c, 0x02, 0x82, 0x7c},
    {0x08, 0x18, 0x28, 0x48, 0xfe, 0x08, 0x08},
    {0xfe, 0x80, 0x80, 0xfc, 0x02, 0x82, 0x7c},
    {0x7c, 0x82, 0x80, 0xfc, 0x82, 0x82, 0x7c},
    {0xfe, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40},
    {0x7c, 0x82, 0x82, 0x7c, 0x82, 0x82, 0x7c},
    {0x7c, 0x82, 0x82, 0x7e, 0x02, 0x82, 0x7c}
};

#define BULLET_SPRITE 2
#define PLAYER_BULLET_SHIFT 11

uint16_t sprites[3][64] PROGMEM = {
    {
    0x0000, 0x0000, 0x0000, 0x001f, 0x001f, 0x0000, 0x0000, 0x0000,
    0x0000, 0x0000, 0x0010, 0x001f, 0x001f, 0x0010, 0x0000, 0x0000,
    0x0000, 0x0010, 0x001f, 0x7eff, 0x7eff, 0x001f, 0x0010, 0x0000,
    0x0010, 0x001f, 0x7eff, 0x7eff, 0x7eff, 0x7eff, 0x001f, 0x0010,
    0x001f, 0x001f, 0x001f, 0x001f, 0x001f, 0x001f, 0x001f, 0x001f,
    0x001f, 0x001f, 0x001f, 0x7eef, 0x7eef, 0x001f, 0x001f, 0x001f,
    0x001f, 0x001f, 0x7eef, 0xffe0, 0xffe0, 0x7eef, 0x001f, 0x001f,
    0x0000, 0x0000, 0x0000, 0xf800, 0xf800, 0x0000, 0x0000, 0x0000
    },
    {
    0x0000, 0x0000, 0x7bef, 0xffff, 0xffff, 0x7bef, 0x0000, 0x0000,
    0xfe05, 0xf800, 0xf800, 0xf800, 0xf800, 0xf800, 0xf800, 0xfe05, 
    0xf000, 0xf800, 0xfe05, 0xf800, 0xf800, 0xfe05, 0xf800, 0xf000,
    0x0000, 0x0000, 0xf000, 0xf800, 0xf800, 0xf000, 0x0000, 0x0000,
    0x0000, 0xf000, 0xf800, 0xf800, 0xf800, 0xf800, 0xf000, 0x0000,
    0xf800, 0xf800, 0x7bef, 0xaeb5, 0xaeb5, 0x7bef, 0xf800, 0xf800,
    0x0000, 0x0000, 0xf800, 0xffff, 0xffff, 0xf800, 0x0000, 0x0000,
    0x0000, 0x0000, 0xaeb5, 0xffff, 0xffff, 0xaeb5, 0x0000, 0x0000
    },
    {
    0x0000, 0x0000, 0xf800, 0xffff, 0xf800, 0x0000, 0x0000, 0x0000,
    0x0000, 0x0000, 0x0000, 0xf800, 0xffff, 0xf800, 0x0000, 0x0000,
    0x0000, 0x0000, 0x0000, 0x0000, 0xf800, 0xffff, 0xf800, 0x0000,
    0x0000, 0x0000, 0x0000, 0xf800, 0xffff, 0xf800, 0x0000, 0x0000,
    0x0000, 0x0000, 0xf800, 0xffff, 0xf800, 0x0000, 0x0000, 0x0000,
    0x0000, 0xf800, 0xffff, 0xf800, 0x0000, 0x0000, 0x0000, 0x0000, 
    0x0000, 0x0000, 0xf800, 0xffff, 0xf800, 0x0000, 0x0000, 0x0000,
    0x0000, 0x0000, 0x0000, 0xf800, 0xffff, 0xf800, 0x0000, 0x0000
    }
};

void draw_sprite(int x, int y, uint8_t n, uint8_t s = 2, uint8_t sh = 0)
{
    uint16_t *ptr = sprites[n];
    sh = sh % 16;

    uint8_t i = 0;

    if (y <= 0) {
        if (y < -SH0 * s)
            return;
        i = -y/s;
    }

    while (i < 8) {

        LCD::setArea(x, y + s * i, x + s * SW0 - 1, y + s * i + 1);
        CS0;
        for (uint8_t ii = 0; ii < s; ++ii) {
            for (uint8_t j = 0; j < 8; ++j) {
                uint16_t word = pgm_read_word(ptr + (i * 8) + j);
                uint16_t high, low;
                if (sh != 0) {
                    high = (((word >> sh) | ((word << (16 - sh)))) >> 8) & 0xff;
                    low = ((word >> sh) | ((word << (16 - sh)))) & 0xff;
                } else {
                    high = word >> 8;
                    low = word & 0xff;
                }
                for (int jj = 0; jj < s; ++jj) {
                    LCD_SET_DATH(high);
                    LCD_SET_DATL(low);
                    WR0;
                    WR1;
                }
            }
        }
        CS1;
        i++;
    }
}

void clear_sprite(int x, int y, uint8_t s = 2)
{
    int y2 = y + SH0 * s - 1;

    if (y <= 0) {
        if (y < -SH0 * s)
            return;
        y = 0;
    }

    LCD::setArea(x, y, x + SW0 * s - 1, y2);
    LCD::fill(SW0 * s, SH0 * s, BLACK);
}

void draw_char(int x, int y, uint8_t set[][7], uint8_t c, uint8_t s, short sh = -1, uint16_t mask = 0xe71c)
{
    LCD::setArea(x, y, x + 7 * s - 1, y + 7 * s - 1);
    CS0;
    uint8_t *ptr = set[c];

    for (int i = 0; i < 7; ++i) {
        for (int ii = 0; ii < s; ++ii) {
            uint8_t byte = pgm_read_byte(ptr + i);
            for (int j = 0; j < 7; ++j) {
                for (int jj = 0; jj < s; ++jj) {
                    if (byte & 128) {
                        if (sh > 0) {
                            int word = 0xffff ^ (mask >> sh);
                            LCD_SET_DATH(word >> 8);
                            LCD_SET_DATL(word & 0xff);
                        } else {
                            LCD_SET_DATH(0xff);
                            LCD_SET_DATL(0xff);
                        }
                    } else {
                        LCD_SET_DATH(0x0);
                        LCD_SET_DATL(0x0);
                    }
                    WR0;
                    WR1;
                }
                byte = byte << 1;
            }
        }
        if (sh != -1)
            sh = (sh + 1) % 5;
    }
    CS1;
}

#define STARS 32

uint8_t stars[STARS][2] = {
    {0,0}, {0,0}, {0,0}, {0,0}, {0,0}, {0,0}, {0,0}, {0,0},
    {0,0}, {0,0}, {0,0}, {0,0}, {0,0}, {0,0}, {0,0}, {0,0},
    {0,0}, {0,0}, {0,0}, {0,0}, {0,0}, {0,0}, {0,0}, {0,0},
    {0,0}, {0,0}, {0,0}, {0,0}, {0,0}, {0,0}, {0,0}, {0,0},
    };

void plot_stars()
{
    for (uint8_t i = 0; i < STARS; ++i) {
        uint8_t sx = stars[i][0];
        uint16_t sy = stars[i][1] * 2;

        LCD::setArea(sx, sy, sx, sy);
        CS0;
        LCD_SET_DATH(0x0);
        LCD_SET_DATL(0x0);
        WR0;
        WR1;
        CS1;

        sy = (sy + 2) % 320;

        if ((sx + sy) % 64 < 32) {
            uint16_t r = (sx + sy) % 0x1f;
            uint16_t g = (((sx % 32) + sy) % 0x1f) << 1;
            uint16_t b = ((sx % 64) + sy) % 0x1f;
            uint16_t rgb = r << 11 | g << 5 | b;

            LCD::setArea(sx, sy, sx, sy);
            CS0;
            LCD_SET_DATH(rgb >> 8);
            LCD_SET_DATL(rgb & 0xff);
            WR0;
            WR1;
            CS1;
        }

        stars[i][1] = sy / 2;
    }
}

uint16_t scores[8] = {4096, 2048, 1024, 512, 256, 128, 64, 32};
uint16_t names[8] = {0x5824, 0x5824, 0x5824, 0x5824, 0x5824, 0x5824, 0x5824, 0x5824};

uint8_t x = 112;
uint8_t tx = 112;
bool fire = false;
uint16_t score;
uint8_t lives;

uint8_t exploding = 0;
int b[2][2] = {{-1,-1}, {-1,-1}};

int a[ALIENS][2] = {
    {-1,-1}, {-1,-1}, {-1,-1}, {-1,-1}, {-1,-1}, {-1,-1},
    {-1,-1}, {-1,-1}, {-1,-1}, {-1,-1}, {-1,-1}, {-1,-1},
    {-1,-1}, {-1,-1}, {-1,-1}, {-1,-1}
    };
short at[ALIENS][2] = {
    {-1,-1}, {-1,-1}, {-1,-1}, {-1,-1}, {-1,-1}, {-1,-1},
    {-1,-1}, {-1,-1}, {-1,-1}, {-1,-1}, {-1,-1}, {-1,-1},
    {-1,-1}, {-1,-1}, {-1,-1}, {-1,-1}
    };
int ab[4][2] = {{-1,-1}, {-1,-1}, {-1,-1}, {-1,-1}};
uint8_t dive_limit;

#define alien_position(AX, D, MULTIPLIER) \
    if (D < 8) \
        AX = AX + (4 - D)/2 * MULTIPLIER; \
    else \
        AX = AX + (D - 12)/2 * MULTIPLIER;

#define destroy_alien(J) \
    at[J][0] = 97; \
    score += (at[J][1] + 1); \
    uint16_t s = score >> 5; \
    dive_limit = 32; \
    while (s > 0) { \
        s = s >> 2; \
        dive_limit -= 1; \
    }

void move_aliens()
{
    static uint8_t acount = 0;
    bool added = false;
    static uint8_t bullet_counter = 0;

    acount = (acount + 1) % 16;
    bullet_counter = (bullet_counter + 1) % 32;

    for (uint8_t i = 0; i < ALIENS; ++i) {

        short t = at[i][1];

        if (t != -1) {

            int ax = a[i][0];
            int ay = a[i][1];
            short d = at[i][0];

            if (d < 16) {
                clear_sprite(ax, ay);

                d = (d + 1) % 16;
                alien_position(ax, d, 1);
                at[i][0] = d;

                a[i][0] = ax;
                draw_sprite(ax, ay, 1, 2, t * 5);

                // Only let the bottom row or aliens (0 - 3) or those with
                // no aliens beneath them fire bullets. Only fire when the
                // player's spaceship is not exploding.
                if (!exploding && bullet_counter == i) {

                    short j = i - ROW_ITEMS;
                    while (j >= 0) {
                        if (at[j][1] == -1)
                            j -= ROW_ITEMS;
                        else
                            break;
                    }

                    if (j < 0) {
                        uint8_t action = random() % 32;
                        if (action < 10 && ab[i % ALIEN_BULLETS][0] == -1) {

                            // Fire a bullet.
                            ab[i % ALIEN_BULLETS][0] = ax + SW0/2;
                            ab[i % ALIEN_BULLETS][1] = ay + SH;
                            draw_sprite(ab[i % ALIEN_BULLETS][0],
                                        ab[i % ALIEN_BULLETS][1],
                                        BULLET_SPRITE, 1);

                        } else if (action >= dive_limit) {
                            // Dive at the player.
                            at[i][0] = 33;
                        }
                    }
                }
            }
            else if (d == 32)
            {
                clear_sprite(ax, ay);
                ay += 4;

                if (ay == ROW_HEIGHT * (ALIEN_ROWS - (i / ROW_ITEMS) + 1))
                    at[i][0] = 0;

                draw_sprite(ax, ay, 1, 2, t * 5);
                a[i][1] = ay;
            }
            else if (d < 97)
            {
                // Types of 33 to 96 indicate that the alien is diving.
                clear_sprite(ax, ay);
                ay += 2;

                d = d - 33;
                alien_position(ax, (d % 64) / 4, 5);
                at[i][0] = 33 + ((d + 1) % 64);

                if (ay >= 320) {
                    ax = 120 + (((i % ROW_ITEMS) - ROW_ITEMS/2) * ROW_SPACING);
                    ay = -SH;
                    at[i][0] = 32; // descend into place again

                } else if (!exploding) {
                    if (ay > (Y - SH) && ay < (Y + SH) &&
                        ax > (x - SW) && ax < (x + SW)) {
                    
                        destroy_alien(i);
                        exploding = 1;
                    }
                }

                a[i][0] = ax;
                a[i][1] = ay;
                draw_sprite(ax, ay, 1, 2, t * 5);
            }
            else if (d < 113) {
                // Types of 97 to 113 indicate that the alien is exploding.
                draw_sprite(ax, ay, 1, 2, (t * 5) + d - 97);
                at[i][0] = d + 1;

            } else {
                // All other types mean that the alien has been destroyed.
                clear_sprite(ax, ay);
                at[i][1] = -1;
            }

        } else if (!added && acount == 0) {
            // Every 32 frames add a new alien to this space if there are
            // empty spaces above it.
            short j = i + ROW_ITEMS;

            while (j < ALIENS) {
                if (at[j][1] == -1)
                    j += ROW_ITEMS;
                else
                    break;
            }

            if (j >= ALIENS) {
                a[i][0] = 120 + (((i % ROW_ITEMS) - ROW_ITEMS/2) * ROW_SPACING);
                a[i][1] = -SH;
                at[i][0] = 32; // descend into place
                at[i][1] = (i / ROW_ITEMS) % 4;
                added = true;
            }
        }
    }
}

void move_bullets()
{
    // Alien bullets
    for (uint8_t i = 0; i < ALIEN_BULLETS; ++i) {
        int bx = ab[i][0];
        if (bx == -1)
            continue;

        int by = ab[i][1];
        clear_sprite(bx, by, 1);

        by += 4;
        if (by > 280 && by < 304 && bx > x - SW0 && bx < x + SW) {
            // The bullet hit the player's spaceship.
            ab[i][0] = -1;
            exploding = 1;
        } else if (by < 320) {
            // The bullet is still on the screen.
            draw_sprite(bx, by, BULLET_SPRITE, 1);
            ab[i][1] = by;
        } else
            // The bullet has left the screen.
            ab[i][0] = -1;
    }

    // Spaceship bullets
    for (uint8_t i = 0; i < BULLETS; ++i) {
        int bx = b[i][0];
        if (bx == -1)
            continue;

        int by = b[i][1];
        clear_sprite(bx, by, 1);

        by -= 4;
        if (by <= -SH0)
            b[i][0] = -1;
        else {
            for (uint8_t j = 0; j < ALIENS; ++j) {

                // If the alien is not present or is already exploding, skip it.
                if (at[j][1] == -1 || at[j][0] >= 97)
                    continue;

                int ax = a[j][0];
                int ay = a[j][1];

                if (by > ay - SH0 && by < (ay + SH) && bx > (ax - SW0) && bx < (ax + SW)) {
                    b[i][0] = -1;
                    destroy_alien(j);
                    break;
                }
            }
            if (b[i][0] != -1) {
                draw_sprite(bx, by, BULLET_SPRITE, 1, PLAYER_BULLET_SHIFT);
                b[i][1] = by;
            }
        }
    }
}

void move_spaceship()
{
    static uint8_t reload = 0;

    if (exploding > 0) {
        draw_sprite(x, Y, 0, 2, exploding);
        exploding += 1;

        if (exploding == 64) {
            clear_sprite(x, Y);
            lives -= 1;
            clear_sprite((lives - 1) * 8, 0, 1);
            x = 112;
            tx = 112;
            exploding = 0;
        }
    } else {
        if (reload > 0)
            reload -= 1;

        if (x < tx) {
            clear_sprite(x, Y);
            x += 4;
        } else if (x > tx) {
            clear_sprite(x, Y);
            x -= 4;
        }
        draw_sprite(x, Y, 0);

        if (fire && reload == 0) {
            for (uint8_t i = 0; i < BULLETS; ++i) {
                if (b[i][0] == -1) {
                    b[i][0] = x + SW0/2;
                    b[i][1] = Y - SH0;
                    draw_sprite(b[i][0], b[i][1], BULLET_SPRITE, 1, PLAYER_BULLET_SHIFT);
                    reload = 31;
                    break;
                }
            }
            fire = false;
        }
    }
}

#define draw_number(c, y, s, scale, shift) \
    uint8_t d = 0; \
    while (d < 5) { \
        uint8_t index = s % 10; \
        draw_char((c - d) * (8 * scale), y, numbers, index, scale, shift); \
        s = s / 10; \
        d++; \
    }

void draw_score_and_lives(uint16_t score, uint8_t lives)
{
    uint16_t s = score;
    uint8_t c = 29;

    draw_number(c, 0, s, 1, -1)

    for (c = 0; c < lives - 1; ++c)
        draw_sprite(c * 8, 0, 0, 1);
}

POINT *read_point()
{
    static POINT previous_tp;
    static POINT *p;
    static uint8_t count = 0;

    p = tp.read_lcd_point();

    if (p->x == 0 || p->y == 0)
        return 0;

    if (p->x != previous_tp.x || p->y != previous_tp.y) {
        count = 0;
        previous_tp.x = p->x;
        previous_tp.y = p->y;
    }
    else
        count++;

    if (count >= 2) {
        count = 0;
        fire = true;
        return p;
    }

    return 0;
}

unsigned long previousTime = 0;

void title_loop()
{
    LCD::setArea(0, 0, 239, 319);
    LCD::fill(240, 320, BLACK);

    uint8_t sh = 0;

    for (;;) {

        unsigned long currentTime = millis();
        if (currentTime < previousTime + 40)
            continue;

        previousTime = currentTime;

        plot_stars();

        for (uint8_t i = 0; i < 8; ++i)
            draw_char(TITLE_X1 + (i * 16), TITLE_Y1, chars, pgm_read_byte(title1 + i) - 65, 2, sh, 0xf9ff);

        for (uint8_t i = 0; i < 4; ++i)
            draw_char(TITLE_X2 + (i * 16), TITLE_Y2, chars, pgm_read_byte(title2 + i) - 65, 2, sh, 0x5e7f);

        for (uint8_t i = 0; i < 5; ++i)
            draw_char(TITLE_X3 + (i * 16), TITLE_Y3, chars, pgm_read_byte(title3 + i) - 65, 2, sh, 0xe7fe);

        for (uint8_t i = 0; i < 8; ++i) {
            uint8_t c = 7;
            uint16_t s = scores[i];
            draw_number(c, 8 + (i + 3) * 24, s, 2, (sh + 1) % 5);
        }

        for (uint8_t i = 0; i < 8; ++i) {
            uint16_t name = names[i];
            for (uint8_t j = 0; j < 3; ++j) {
                uint16_t c = (name & 0x1f);
                if (c > 0)
                    draw_char(144 + (j * 16), 8 + (i + 3) * 24, chars, c - 1, 2, (sh + 1) % 5, 0xe7ff);
                else
                    break;
                name = name >> 5;
            }
        }

        sh = (sh + 1) % 5;

        if (POINT *p = read_point())
            break;
    }
}

#define NAME_X 80
#define NAME_Y 104
#define FINISHED_X 144
#define FINISHED_TX 138
#define FINISHED_TY 98
#define KBD_X 28
#define KBD_Y 176
#define KBD_TX 22
#define KBD_TY 170
#define KBD_SX 28
#define KBD_SY 28

void record_score(uint8_t position)
{
    LCD::setArea(0, 0, 239, 319);
    LCD::fill(240, 320, BLACK);

    uint8_t c = 0;
    uint16_t name[3] = {0, 0, 0};
    uint8_t sh = 0;
    uint8_t press = 0;

    for (;;) {

        unsigned long currentTime = millis();
        if (currentTime < previousTime + 40)
            continue;

        previousTime = currentTime;

        plot_stars();

        for (uint8_t i = 0; i < 5; ++i)
            draw_char(80 + (i * 16), 16, chars, pgm_read_byte(title4 + i) - 65, 2, sh, 0x5a5d);
        for (uint8_t i = 0; i < 4; ++i) {
            draw_char(48 + (i * 16), 36, chars, pgm_read_byte(title5 + i) - 65, 2, sh, 0x5a5a);
            draw_char(128 + (i * 16), 36, chars, pgm_read_byte(title6 + i) - 65, 2, sh, 0x5ada);
        }

        for (uint8_t i = 0; i < 27; ++i)
            draw_char(KBD_X + (i % 7) * KBD_SX, KBD_Y + (i / 7) * KBD_SY, chars, i, 2, sh, 0x55ad);

        for (uint8_t l = 0; l < 3; ++l) {
            if (name[l] != 0) {
                if (c == l)
                    draw_char(NAME_X + (l * 16), NAME_Y, chars, name[l] - 1, 2, sh);
                else
                    draw_char(NAME_X + (l * 16), NAME_Y, chars, name[l] - 1, 2);
            } else if (c == l)
                draw_char(NAME_X + (l * 16), NAME_Y, chars, 28, 2, sh);
        }

        draw_char(FINISHED_X, NAME_Y, chars, 27, 2, sh, 0xf7cf);

        if (POINT *p = read_point()) {
            if (press == 0) {
                // Adjust the top-left corner of the letters region to displace
                // the boundaries between letters away from the top-left corners
                // of the letters.
                uint8_t j = ((p->y - KBD_TX) / KBD_SX);
                uint8_t i = ((p->x - KBD_TY) / KBD_SY);

                if (i < 4 && j < 7) {
                    uint8_t ch = (i * 7) + j + 1;
                    if (ch < 27) {
                        name[c] = ch;
                        if (c < 2)
                            c++;
                    } else if (ch == 27) {
                        name[c] = 0;
                        clear_sprite(NAME_X + (c * 16), NAME_Y, 2);
                        if (c != 0)
                            c--;
                    }
                } else if ((p->y - FINISHED_TX)/KBD_SX == 0 &&
                           (p->x - FINISHED_TY)/KBD_SY == 0)
                    break;

                press = 15;
            }
        }

        if (press > 0)
            press--;
        sh = (sh + 1) % 5;
    }

    scores[position] = score;
    names[position] = name[0] | (name[1] << 5) | (name[2] << 10);
}

void game_loop()
{
    LCD::setArea(0, 0, 239, 319);
    LCD::fill(240, 320, BLACK);

    score = 0;
    lives = 3;
    dive_limit = 32;

    for (uint8_t i = 0; i < ALIENS; ++i)
        at[i][1] = -1;

    while (lives > 0) {

        if (POINT *p = read_point()) {
            tx = p->y;
            if (tx > 224)
                tx = 224;
            int r = (tx % 4);
            if (r != 0)
                tx -= r;
        }

        unsigned long currentTime = millis();
        if (currentTime < previousTime + 40)
            continue;

        previousTime = currentTime;

        plot_stars();
        move_aliens();
        move_bullets();
        move_spaceship();
        draw_score_and_lives(score, lives);
    }

    for (uint8_t i = 0; i < 8; ++i) {
        if (score > scores[i]) {
            for (uint8_t j = 7; j > i; --j) {
                scores[j] = scores[j - 1];
                names[j] = names[j - 1];
            }
            record_score(i);
            break;
        }
    }
}

void setup()
{
    LCD::init(LCD_VERTICAL);
    LCD::resetArea();
    LCD::fill(LCD::GetWidth() - 1, LCD::GetHeight() - 1, BLACK);
    tp.init();

    srandom(255);
    for (uint8_t i = 0; i < STARS; ++i) {
        stars[i][0] = random() % 240;
        stars[i][1] = random() % 160;
    }
}

int main(void)
{
    init();

    setup();

    for (;;) {
        title_loop();
        game_loop();
    }
    
    return 0;
}
