#!/usr/bin/env python

# Copyright (C) 2010 David Boddie <david@boddie.org.uk>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Adapted from:
#
# Arduino 0011 Makefile
# Arduino adaptation by mellis, eighthave, oli.keller

import commands, os, shutil, stat, sys
import pulse_serial

TARGET = "invaders"
PORT = "/dev/ttyUSB0"
UPLOAD_RATE = 19200
AVRDUDE_PROGRAMMER = "stk500v1"
MCU = "atmega168"
F_CPU = 16000000

AVR_TOOLS_PATH = "/usr/bin"
TFT_DIR = "../../TFT_lib"
ARDUINO_DIR = "/data2/david/Software/Arduino/arduino-0012/hardware/cores/arduino"

def _tft(paths):
    return map(lambda path: os.path.join(TFT_DIR, path), paths)

def _arduino(paths):
    return map(lambda path: os.path.join(ARDUINO_DIR, path), paths)

def _tool(path):
    return os.path.join(AVR_TOOLS_PATH, path)

def _system(*args):
    command_line = " ".join(args)
    print command_line
    if os.system(command_line) != 0:
        sys.exit(1)

SOURCES = _arduino(
    ["pins_arduino.c", "wiring.c", "wiring_analog.c",
     "wiring_digital.c", "wiring_pulse.c", "wiring_serial.c",
     "wiring_shift.c", "WInterrupts.c"])
# "HardwareSerial.cpp", "Print.cpp", "WMath.cpp"

SOURCES += _tft(
    ["Touchpanel.cpp", "TFT_ILI9325.cpp"])

SOURCES += ["main.cpp"]

FORMAT = "ihex"

# Debugging format.
# Native formats for AVR-GCC's -g are stabs [default], or dwarf-2.
# AVR (extended) COFF requires stabs, plus an avr-objcopy run.
DEBUG = "stabs"

OPT = "s"

# Place -D or -U options here
CDEFS = CXXDEFS = "-DF_CPU=%i" % F_CPU

# Place -I options here
CINCS = CXXINCS = "-I%s -I%s" % (ARDUINO_DIR, TFT_DIR)

# Compiler flag to set the C Standard level.
# c89   - "ANSI" C
# gnu89 - c89 plus GCC extensions
# c99   - ISO C99 standard (not yet fully implemented)
# gnu99 - c99 plus GCC extensions
CSTANDARD = "-std=gnu99"
CDEBUG = "-g%s" % DEBUG
CWARN = "-Wall -Wstrict-prototypes"
CTUNING = "-funsigned-char -funsigned-bitfields -fpack-struct -fshort-enums"
CEXTRA = ""

CFLAGS = "%s %s %s -O%s %s %s %s" % (
    CDEBUG, CDEFS, CINCS, OPT, CWARN, CSTANDARD, CEXTRA)
CXXFLAGS = "%s %s -O%s" % (CDEFS, CINCS, OPT)
ASFLAGS = "-Wa,-adhlns=$(<:.S=.lst),-gstabs "
LDFLAGS = "-lm"


# Programming support using avrdude. Settings and variables.
AVRDUDE_PORT = PORT
AVRDUDE_WRITE_FLASH = "-U flash:w:%s.hex" % TARGET
AVRDUDE_FLAGS = (
    "-V -F -C /etc/avrdude.conf "
    "-p %s -P %s -c %s "
    "-b %s") % (MCU, AVRDUDE_PORT, AVRDUDE_PROGRAMMER, UPLOAD_RATE)

# Program settings
CC = _tool("avr-gcc")
CXX = _tool("avr-g++")
OBJCOPY = _tool("avr-objcopy")
OBJDUMP = _tool("avr-objdump")
AR  = _tool("avr-ar")
SIZE = _tool("avr-size")
NM = _tool("avr-nm")
STRIP = _tool("avr-strip")
AVRDUDE = _arduino(["../../tools/avrdude"])[0]

# Define all object files.
mappings = {
    ".c": ".o",
    ".cpp": ".o",
    ".S": ".o"
    }

OBJECTS = []
for path in SOURCES:
    stem, suffix = os.path.splitext(path)
    try:
        OBJECTS.append(stem + mappings[suffix])
    except KeyError:
        sys.stderr.write("No mapping for source file: %s\n" % path)
        sys.exit(1)

# Combine all necessary flags and optional flags.
# Add target processor to flags.
ALL_CFLAGS = "-mmcu=%s -I. %s" % (MCU, CFLAGS)
ALL_CXXFLAGS = "-mmcu=%s -I. %s" % (MCU, CXXFLAGS)
ALL_ASFLAGS = "-mmcu=%s -I. -x assembler-with-cpp %s" % (MCU, ASFLAGS)

def build_target():
    # Link: create ELF output file from library.
    _system(CC, ALL_CFLAGS, "-o", TARGET + ".elf", "-L.", LDFLAGS, TARGET + ".a")

def archive_objects():
    for i in OBJECTS:
        _system(AR, "rcs", TARGET + ".a", i)

def strip_target():
    # Link: create ELF output file from library.
    _system(STRIP, TARGET + ".elf")

def upload_target():
    pulse_serial.pulse(PORT, UPLOAD_RATE, 100)
    _system(AVRDUDE, AVRDUDE_FLAGS, AVRDUDE_WRITE_FLASH)

# Display size of file.
HEXSIZE = (SIZE, "--target="+FORMAT, TARGET + ".hex")
ELFSIZE = (SIZE, TARGET + ".elf")

def sizebefore():
    if os.path.exists(TARGET + ".elf"):
        print
        _system(*HEXSIZE)
        print

def sizeafter():
    if os.path.exists(TARGET + ".elf"):
        print
        _system(*HEXSIZE)
        print


# Convert ELF to COFF for use in debugging / simulating in AVR Studio or VMLAB.
def convert_elf_coff():
    _system(OBJCOPY, "--debugging", "--change-section-address",
            ".data-0x800000", "--change-section-address", ".bss-0x800000",
            "--change-section-address", ".noinit-0x800000",
            "--change-section-address", ".eeprom-0x810000",
            "-O", "coff-avr", TARGET + ".elf", TARGET +".cof")

def convert_elf_extcoff():
    _system(OBJCOPY, "--debugging", "--change-section-address",
            ".data-0x800000", "--change-section-address", ".bss-0x800000",
            "--change-section-address", ".noinit-0x800000",
            "--change-section-address", ".eeprom-0x810000",
            "-O", "coff-ext-avr", TARGET + ".elf", TARGET +".cof")


def elf_to_hex(source, target):
    _system(OBJCOPY, "-O", FORMAT, "-R", ".eeprom", source, target)

def elf_to_eep(source, target):
    _system(OBJCOPY, "-j", ".eeprom", '--set-section-flags=.eeprom="alloc,load"',
            "--change-section-lma", ".eeprom=0", "-O", FORMAT, source, target)

# Create extended listing file from ELF output file.
def elf_to_lss(source, target):
    _system(OBJDUMP, "-h", "-S", source, ">", target)

# Create a symbol table from ELF output file.
def elf_to_sym(source, target):
    _system(NM, "-n", source, ">", target)

# Compile: create object files from C++ source files.
def cpp_to_o(source, target):
    _system(CXX, "-c", ALL_CXXFLAGS, source, "-o", target)

# Compile: create object files from C source files.
def c_to_o(source, target):
    _system(CC, "-c", ALL_CFLAGS, source, "-o", target)

# Compile: create assembler files from C source files.
def c_to_s(source, target):
    _system(CC, "-S", ALL_CFLAGS, source, "-o", target)

# Assemble: create object files from assembler source files.
def S_to_o(source, target):
    _system(CC, "-c", ALL_ASFLAGS, source, "-o", target)


# Target: clean project.
def clean():
    for path in OBJECTS + [TARGET + ".hex", TARGET + ".elf", TARGET + ".a"]:
        if os.path.exists(path):
            os.remove(path)

def depend():
    pass

dependencies = {
    "all": ("build", "sizeafter"),
    "build": ("hex",),
    "hex": ("elf", TARGET + ".hex",),
    "elf": (TARGET + ".elf",),
    "eep": (TARGET + ".eep",),
    "lss": (TARGET + ".lss",),
    "sym": (TARGET + ".sym",),
    
    # Program the device.  
    "upload": (TARGET + ".hex",),
    "clean": (),
    "sizeafter": (TARGET + ".hex",),
    
    TARGET + ".elf": (TARGET + ".a",),
    TARGET + ".a": OBJECTS,
    "coff": (TARGET + ".elf",),
    "extcoff": (TARGET + ".elf",),
    }

pattern_rules = {
    ".hex": ((".elf", elf_to_hex),),
    ".eep": ((".elf", elf_to_eep),),
    ".lss": ((".elf", elf_to_lss),),
    ".sym": ((".elf", elf_to_sym),),
    ".o":   ((".cpp", cpp_to_o), (".c", c_to_o), (".S", S_to_o)),
    ".s":   ((".c", c_to_s),),
    }

actions = {
    TARGET + ".a": (archive_objects,),
    TARGET + ".elf": (build_target, strip_target), # convert_elf_coff, convert_elf_extcoff
    "sizebefore": (sizebefore,),
    "sizeafter": (sizeafter,),
    "upload": (upload_target,),
    "clean": (clean,),
    }

def process(rule, processed, indent = 0):

    if rule in processed:
        return
    
    processed.add(rule)
    
    if dependencies.has_key(rule):
    
        requirements = dependencies[rule]
        
        for requirement in requirements:
            process(requirement, processed, indent + 1)
    
    else:
        stem, suffix = os.path.splitext(rule)
        
        try:
            pairs = pattern_rules[suffix]
        except KeyError:
            sys.stderr.write("Cannot handle rule: %s\n" % rule)
            sys.exit(1)
        
        for source_suffix, function in pairs:
        
            source = os.path.abspath(stem + source_suffix)
            target = os.path.abspath(rule)
            
            if not os.path.exists(source):
                continue
            
            if not os.path.exists(target):
                function(source, target)
            else:
                source_time = os.stat(source)[stat.ST_MTIME]
                target_time = os.stat(target)[stat.ST_MTIME]
                
                if source_time > target_time:
                    function(source, target)
    
    if actions.has_key(rule):
    
        rule_actions = actions[rule]
        for action in rule_actions:
            action()

if __name__ == "__main__":

    if len(sys.argv) < 2:
    
        sys.stderr.write("Usage: %s <rule> ...\n" % sys.argv[0])
        for key in dependencies.keys():
            sys.stderr.write(key + "\n")
        sys.exit(1)
    
    rules = sys.argv[1:]
    processed = set()
    
    for rule in rules:
        process(rule, processed)
    
    sys.exit()
