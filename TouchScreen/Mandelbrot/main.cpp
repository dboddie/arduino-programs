/*
    main.cpp - Main program for the Mandelbrot example.

    Adapted from the tp_calibrate example from the TFT_lib package.

    Copyright (C) 2010 David Boddie <david@boddie.org.uk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
    Requires that the touchscreen has been calibrated and the calibration
    matrix has been written to the Arduino's EEPROM using TFT_lib's
    touchpanel.save_matrix() method.
*/

#include "WProgram.h"
#include <TFT_ILI9325.h>
#include <TFT_Graphics.h>
#include <Touchpanel.h>
#include <f12x12.h>
#include <stdlib.h>
#include <avr/pgmspace.h>

#define TP_STABLE_COUNT 3

Graphics tft;
touchpanel tp;

// 16-bit colour
// components are f8   (red)
//                 7e  (green)
//                  1f (blue)

long colours[16] PROGMEM = {0x0001, 0x0002, 0x0823, 0x1044,
                            0x1865, 0x2086, 0x28a7, 0x30c8,
                            0x38e9, 0x410a, 0x492b, 0x514c,
                            0x596d, 0x618e, 0x69af, 0x71df};

void draw_mandelbrot(int x1, int y1, int x2, int y2,
                     double r1, double i1, double r2, double i2)
{
    for (int y = y1; y < y2; ++y) {
        double i = i1 + y * (i2 - i1)/(y2 - y1);
        for (int x = x1; x < x2; ++x) {
            double r = r1 + x * (r2 - r1)/(x2 - x1);
            double tr = r;
            double ti = i;

            int count = 0;
            while (count < 20) {
                double temp = tr*tr - ti*ti + r;
                ti = 2*tr*ti + i;
                tr = temp;
                if ((tr*tr + ti*ti) > 4)
                    break;

                count += 1;
            }

            if (count < 16)
                LCD::putPixel(x, y, pgm_read_word(colours + count));
            else
                LCD::putPixel(x, y, 0);
        }
    }
}

void setup()
{
    tft.initialize(LCD_HORIZONTAL);
    tft.ClearScreen(BLACK);
    tp.init();

    draw_mandelbrot(0, 0, 320, 240, -2.4, -1.2, 0.8, 1.2);
}

int main(void)
{
    init();

    setup();
    
    return 0;
}
