#!/usr/bin/env python

# Copyright (C) 2010 David Boddie <david@boddie.org.uk>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import struct, sys
import Image

if __name__ == "__main__":

    if len(sys.argv) != 3:
    
        sys.stderr.write("Usage: %s <image file> <output file>\n" % sys.argv[0])
        sys.exit(1)
    
    input_file = sys.argv[1]
    output_file = sys.argv[2]
    
    im = Image.open(input_file)
    
    if im.size[0] < im.size[1]:
        w = 240
        h = 320
    else:
        w = 320
        h = 240
    
    xs = float(w) / im.size[0]
    ys = float(h) / im.size[1]
    s = min(xs, ys)
    im = im.resize((int(s * im.size[0]), int(s * im.size[1])), Image.ANTIALIAS)
    output_im = Image.new("RGB", (w, h), 0)
    
    dw = (w - im.size[0])/2
    dh = (h - im.size[1])/2
    output_im.paste(im, (dw, dh))
    
    data = im.tostring()
    f = open(output_file, "w")
    i = 0
    while i < len(data):
    
        b, g, r = struct.unpack("<BBB", data[i:i+3])
        bc = int(b * 31.0 / 255.0) << 11
        gc = int(g * 63.0 / 255.0) << 5
        rc = int(r * 31.0 / 255.0)
        f.write(struct.pack("<H", rc | gc | bc))
        i += 3
    
    sys.exit()
