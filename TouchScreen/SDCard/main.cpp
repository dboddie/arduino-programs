/*
    main.cpp - Main program for the SDCard example.

    Copyright (C) 2010 David Boddie <david@boddie.org.uk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "WProgram.h"
#include <TFT_ILI9325.h>
#include <TFT_PFF.h>
#include <stdlib.h>

uint8_t buffer[32];

int display_bytes(uint32_t offset, uint16_t x, uint16_t y)
{
    // Note, using uncasted uint16_t coordinates in the following calculation
    // will lead to truncation to uint16_t values.
    uint32_t ptr = offset + (uint32_t(y) * 320 * 2) + (uint32_t(x) * 2);
    uint16_t block = ptr / 512;
    uint16_t bptr = ptr % 512;
    uint16_t bend = bptr + 32;

    if (bend > 512)
        bend = 512;

    uint16_t count = bend - bptr;
    disk_readp(buffer, block, bptr, count);

    LCD::setArea(320 - x - (count / 2), y, 320 - x - 1, y);
    CS0;
    int i = count - 2;
    while (i >= 0) {
        LCD_SET_DATH(buffer[i+1]);
        LCD_SET_DATL(buffer[i]);
        WR0;
        WR1;
        i -= 2;
    }
    CS1;

    return count / 2;
}

void setup()
{
    LCD::init(LCD_HORIZONTAL);
    LCD::resetArea();
    LCD::fill(LCD::GetWidth() - 1, LCD::GetHeight() - 1, BLACK);

    disk_initialize();
    
    for (int y = 0; y < 240; ++y) {
        int x = 0;
        while (x < 320) {
            x += display_bytes(0, x, y);
        }
    }
}

int main(void)
{
    init();

    setup();

    return 0;
}
