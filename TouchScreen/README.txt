Notes about Licensing
---------------------

The examples in this directory are licensed under the GNU General Public
License (version 3 or later). However, they depend on source code included in
the TFT_lib package from nuelectronics in which some files contain only
copyright statements and others are licensed under a permissive license.

This means that it may not be possible to distribute statically-linked binary
versions of the examples in this directory without violating the copyright of
some files in TFT_lib, even though it may be considered acceptable to create
binary files that combine the example code with the TFT_lib code if the latter
is considered to be a System Library under the definition given in the GPL.

One solution to this problem would be to obtain TFT_lib under a GPL-compatible
license. Another would be to rewrite it and license it appropriately.

TFT_lib
-------

At the time of writing, the TFT_lib package can be obtained from the following
location:

  http://www.nuelectronics.com/download/projects/TFT_lib.zip
