#!/usr/bin/env python

import struct, sys
from serial import Serial

# https://www.maximintegrated.com/en/app-notes/index.mvp/id/3969

def crc_shift(r, b, mask = 0x80):

    while mask != 0:
    
        bit = b & mask
        r = r << 1
        
        if bit:
            r = r | 1
        
        if r & 0x80:
            r = (r & 0x7f) ^ 0x09
        
        mask = mask >> 1
    
    return r

def crc(bytes):

    r = 0
    for b in bytes:
        r = crc_shift(r, b)
    
    r = crc_shift(r, 0, 0x40)
    return (r << 1) | 1

def loop(s):

    while True:
    
        i = raw_input(">")
        
        pieces = i.split()
        command = pieces[0]
        
        if command == "s":      # init SPI
            s.write("s")
            s.flush()
            print s.readline().strip()
        
        elif command == "v":    # check voltage
            s.write("v")
            s.flush()
            print s.readline().strip()
        
        elif command == "i":    # initialize
            s.write("i")
            s.flush()
            print s.readline().strip()
        
        elif command == "o":    # read OCR
            s.write("o")
            s.flush()
            print s.readline().strip()
        
        elif command == "d":    # check voltage (SD cards)
            s.write("d")
            s.flush()
            print s.readline().strip()
        
        elif command == "l":    # set sector size
            size = int(pieces[1], 16)
            low = size & 0xff
            middle = (size >> 8) & 0xff
            high = (size >> 16) & 0xff
            s.write("l" + chr(low) + chr(middle) + chr(high))
            s.flush()
            print s.readline().strip()
        
        elif command == "r":    # read sector
            address = int(pieces[1], 16)
            low = address & 0xff
            middle = (address >> 8) & 0xff
            high = (address >> 16) & 0xff
            s.write("r" + chr(low) + chr(middle) + chr(high))
            s.flush()
            r = s.readline().strip()
            if r == "OK":
                i = 0
                while i < 512:
                    print repr(s.read(1)),
                    i += 1
                print s.readline().strip()
                print s.readline().strip()
            else:
                print r
        
        elif command == "b":
            s.write("b")
            s.flush()
            print s.readline().strip()
        
        elif command == "w":
        
            output = int(pieces[1], 16)
            s.write("w" + chr(output))
            s.flush()
            print s.readline().strip()
    
        elif command == "c":
        
            n = int(pieces[1], 16)
            args = map(lambda x: int(x, 16), pieces[2:6])
            output = "".join(map(chr, [n] + args))
            s.write("c" + output)
            s.flush()
            print s.readline().strip()
    
    s.close()
    
    print
    
    sys.exit()


if __name__ == "__main__":

    if len(sys.argv) != 3:
        sys.stderr.write("Usage: %s <port> <baud rate>\n" % sys.argv[0])
        sys.exit(1)
    
    port = sys.argv[1]
    baud_rate = sys.argv[2]
    
    s = Serial()
    s.setPort(port)
    s.setBaudrate(baud_rate)
    s.open()
    
    #loop(s)
