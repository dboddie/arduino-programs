#!/usr/bin/env python

# Copyright (C) 2010 David Boddie <david@boddie.org.uk>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from arduino_build import _arduino, _tft, main
import arduino_build

#arduino_build.MCU = "atmega328p"
arduino_build.MCU = "atmega168"

TARGET = "readflash"

SOURCES = _arduino(
    ["wiring.c", "wiring_analog.c",
     "wiring_digital.c",
     "HardwareSerial.cpp", "Print.cpp",
     "Stream.cpp", "WString.cpp"]) #, "WMath.cpp"

#SOURCES += _tft(
#    ["Touchpanel.cpp", "TFT_ILI9325.cpp"])

SOURCES += ["main.cpp"]

main(TARGET, SOURCES)
