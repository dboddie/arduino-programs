/*
    main.cpp - Main program for the SDCard example.

    Copyright (C) 2016 David Boddie <david@boddie.org.uk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
         Arduino Duemilanove        Adafruit 2.8" TFT Breakout Board
                          5V        3-5V
                         Gnd        GND
                           2        CLK
                           3        MISO
                           4        MOSI
                           5        Card CS

         Arduino Duemilanove        Sparkfun SD/MMC Breakout
                         3V3        VCC
                         Gnd        GND
                           2        CLK via level shifters
                           3        D0 (DAT0, DO, MISO)
                           4        CMD (DI, MOSI) via level shifters
                           5        D3 (DAT3, CS) via level shifters
*/

#include "Arduino.h"
#include "HardwareSerial.h"
#include <stdlib.h>
#include <avr/pgmspace.h>

#define CLK     2
#define MISO    3
#define MOSI    4
#define CS      5

#define HALF_PERIOD 8

#define START               's'
#define INITIALIZE          'i'
#define CHECK_VOLTAGE       'v'
#define CHECK_VOLTAGE_SD    'd'
#define READ_OCR            'o'
#define SET_SECTOR_SIZE     'l'
#define READ_SECTOR         'r'

#define WRITE_DATA  'w'
#define COMMAND     'c'
#define READ_BUFFER 'b'

uint8_t command_enter_SPI[5] = {0x40, 0, 0, 0, 0};  // R1
uint8_t command_initialize[5] = {0x41, 0, 0, 0, 0}; // R1
uint8_t command_read_OCR[5] = {0x7a, 0, 0, 0, 0};   // R3
uint8_t command_check_voltage[5] = {0x48, 0xaa, 0x01, 0, 0}; // R7
uint8_t command_check_voltage_SD[5] = {0x69, 0, 0, 0, 0}; // R1
uint8_t command_app_cmd[5] = {0x77, 0, 0, 0, 0}; // R1
uint8_t command_set_512_byte_sectors[5] = {0x50, 0, 0, 0, 0};

// CRC documented at https://www.maximintegrated.com/en/app-notes/index.mvp/id/3969

uint8_t crc_shift(uint8_t r, uint8_t b, uint8_t mask)
{
    while (mask != 0) {
    
        uint8_t bit = b & mask;
        r = r << 1;
        
        if (bit)
            r = r | 1;
        
        if (r & 0x80)
            r = (r & 0x7f) ^ 0x09;
        
        mask = mask >> 1;
    }

    return r;
}

uint8_t crc(uint8_t bytes[])
{
    uint8_t r = 0;

    for (int i = 0; i < 5; ++i)
        r = crc_shift(r, bytes[i], 0x80);
    
    r = crc_shift(r, 0, 0x40);
    return (r << 1) | 1;
}

void setup()
{
    Serial.begin(115200);
    Serial.setTimeout(1000);    // milliseconds

    pinMode(CLK, OUTPUT);
    pinMode(MISO, INPUT);
    pinMode(MOSI, OUTPUT);
    pinMode(CS, OUTPUT);

    digitalWrite(CLK, LOW);
    digitalWrite(CS, LOW);

    pinMode(13, OUTPUT);
    digitalWrite(13, LOW);
}

uint8_t transfer(uint8_t value)
{
    uint8_t mask = 0x80;
    uint8_t input = 0;
    
    for (uint8_t mask = 0x80; mask != 0; mask = mask >> 1) {

        digitalWrite(MOSI, ((value & mask) != 0) ? HIGH : LOW);
        delayMicroseconds(HALF_PERIOD);

        digitalWrite(CLK, HIGH);
        input = (input << 1) | (digitalRead(MISO) == LOW ? 0 : 1);

        delayMicroseconds(HALF_PERIOD);
        digitalWrite(CLK, LOW);
    }

    return input;
}

void enterCommandMode()
{
    delay(2);                       // Wait >1ms.
    digitalWrite(CS, HIGH);
    digitalWrite(MOSI, HIGH);

    for (int i = 0; i < 74; ++i) {
        digitalWrite(CLK, HIGH);
        delayMicroseconds(HALF_PERIOD);
        digitalWrite(CLK, LOW);
        delayMicroseconds(HALF_PERIOD);
    }

    digitalWrite(CS, LOW);
}

uint8_t sendCommand(uint8_t bytes[5])
{
    for (int i = 0; i < 5; ++i)
        transfer(bytes[i]);

    transfer(crc(bytes));

    uint8_t r = 0;

    for (int i = 0; i <= 0xff; ++i) {
        // Read until the byte returned has its top bit clear or we time out.
        r = transfer(0xff);
        if ((r & 0x80) == 0)
            return r;
    }

    return r;
}

uint8_t initialize()
{
    uint8_t r = 0xff;
    digitalWrite(13, HIGH);

    for (uint16_t i = 0; i < 0xfff; ++i) {
        r = sendCommand(command_initialize);
        if (r == 0)
            break;
    }

    digitalWrite(13, LOW);
    return r;
}

uint8_t readDataPacket()
{
    uint8_t r;
    do {
        r = transfer(0xff);
        // Return early if we receive an error token (000xxxxx).
        if ((r & 0xe0) == 0)
            return r;
    } while (r != 0xfe);

    Serial.println("OK");

    for (int i = 0; i < 512; ++i)
        Serial.write(transfer(0xff));

    // CRC
    Serial.println(transfer(0xff), HEX);
    Serial.println(transfer(0xff), HEX);
}

uint8_t readOCR(uint8_t buffer[])
{
    uint8_t r = sendCommand(command_read_OCR);
    if (r == 0) {
        // R3: 4 bytes response
        for (int i = 0; i < 4; ++i)
            buffer[i] = transfer(0xff);
    }
}

uint8_t checkVoltage(uint8_t buffer[])
{
    uint8_t r = sendCommand(command_check_voltage);
    if (r == 0) {
        // R7: 4 bytes response
        for (int i = 0; i < 4; ++i)
            buffer[i] = transfer(0xff);
    }

    return r;
}

uint8_t checkVoltageSD(uint8_t buffer[])
{
    uint8_t r = 1;
    
    for (uint16_t i = 0; i < 0xfff && r != 0; ++i) {
        r = sendCommand(command_app_cmd);
        if (r != 0 && r != 1)
            continue;
        r = sendCommand(command_check_voltage_SD);
    }

    return r;
}

int main(void)
{
    char buffer[8];
    uint8_t output[8];
    int offset = -1;

    init();
    setup();
    enterCommandMode();

    while (1) {

        int total = 0;
        while (total < 1)
            total += Serial.readBytes(&buffer[total], 1 - total);

        char command = buffer[0];
        uint8_t r;

        switch (command) {
        case START:
        {
            r = sendCommand(command_enter_SPI);
            if (r == 1)
                Serial.println("OK");
            else {
                Serial.print("Failed: ");
                Serial.println(r, HEX);
            }
            break;
        }
        case INITIALIZE:
        {
            r = initialize();
            if (r == 0)
                Serial.println("OK");
            else {
                Serial.print("Failed: ");
                Serial.println(r, HEX);
            }
            break;
        }
        case READ_OCR:
        {
            r = readOCR(output);
            if (r == 0)
                Serial.println("OK");
            else {
                Serial.print("Failed: ");
                Serial.println(r, HEX);
            }
            break;
        }
        case CHECK_VOLTAGE:
        {
            r = checkVoltage(output);
            if (r == 0)
                Serial.println("OK");
            else {
                Serial.print("Failed: ");
                Serial.println(r, HEX);
            }
            break;
        }
        case CHECK_VOLTAGE_SD:
        {
            r = checkVoltageSD(output);
            if (r == 0)
                Serial.println("OK");
            else {
                Serial.print("Failed: ");
                Serial.println(r, HEX);
            }
            break;
        }
        case SET_SECTOR_SIZE:
        {
            // l<low><middle><high>
            while (total < 4)
                total += Serial.readBytes(&buffer[total], 4 - total);

            output[0] = 0x50;
            output[1] = buffer[3];  // high byte
            output[2] = buffer[2];  // middle byte
            output[3] = buffer[1];  // low byte
            output[4] = 0;

            r = sendCommand(output);
            if (r == 0)
                Serial.println("OK");
            else {
                Serial.print("Failed: ");
                Serial.println(r, HEX);
            }
            break;
        }
        case READ_SECTOR:
        {
            // r<low><middle><high>
            while (total < 4)
                total += Serial.readBytes(&buffer[total], 4 - total);

            output[0] = 0x51;
            output[1] = buffer[3];  // high byte
            output[2] = buffer[2];  // middle byte
            output[3] = buffer[1];  // low byte
            output[4] = 0;

            r = sendCommand(output);
            if (r == 0) {
                r = readDataPacket();
                if (r == 0xfe)
                    break;
            } else {
                Serial.print("Failed: ");
                Serial.println(r, HEX);
            }
            break;
        }
        case READ_BUFFER:
        {
            for (int i = 0; i < 8; ++i) {
                Serial.print(output[i], HEX);
                Serial.print(" ");
            }
            Serial.println();
            break;
        }
        case WRITE_DATA:
        {
            // w<b>
            while (total < 2)
                total += Serial.readBytes(&buffer[total], 2 - total);

            uint8_t input = transfer(buffer[1]);
            Serial.println(input, HEX);
            break;
        }
        case COMMAND:
        {
            // c<n><a0><a1><a2><a3>
            while (total < 6)
                total += Serial.readBytes(&buffer[total], 6 - total);

            uint8_t input = 0;

            r = sendCommand((uint8_t *)&buffer[1]);
            Serial.print("Response: ");
            Serial.println(r, HEX);
            break;
        }
        default:
            break;
        }
    }

    return 0;
}
