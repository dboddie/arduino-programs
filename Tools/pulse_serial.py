#!/usr/bin/env python

# Copyright (C) 2010 David Boddie <david@boddie.org.uk>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys, time
from serial import Serial

def pulse(port, baud_rate, duration):

    s = Serial()
    s.setPort(port)
    s.setBaudrate(baud_rate)
    s.open()
    
    s.setDTR(1)
    time.sleep(duration/1000.0)
    s.setDTR(0)
    time.sleep(duration/1000.0)
    
    s.close()

if __name__ == "__main__":

    if len(sys.argv) != 4:
        sys.stderr.write("Usage: %s <port> <baud rate> <duration in ms>\n" % sys.argv[0])
        sys.exit(1)
    
    try:
        port = sys.argv[1]
        baud_rate = sys.argv[2]
        duration = int(sys.argv[3])
    
    except ValueError:
        sys.stderr.write("Please specify a duration in milliseconds.\n")
        sys.exit(1)
    
    pulse(port, baud_rate, duration)
    sys.exit()
